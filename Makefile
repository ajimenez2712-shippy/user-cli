build:
	docker build -t shippy_user-cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		shippy_user-cli
